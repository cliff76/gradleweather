package com.apress.gerber.gradleweather;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.apress.gerber.weather.parse.WeatherParser;
import com.apress.gerber.weather.request.NationalWeatherRequest;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Clifton
 * Copyright 8/28/2014.
 */
public class NationalWeatherRequestData implements TemperatureData {

    public static final double DEFAULT_LATITUDE = 37.368830;
    public static final double DEFAULT_LONGITUDE = -122.036350;
    private final WeatherParser weatherParser;
    private final Context context;

    public NationalWeatherRequestData(Context context) {
        this.context = context;
        Location location = getLocation(context);
        weatherParser = new WeatherParser();
        String weatherXml = new NationalWeatherRequest(location).getWeatherXml();
        //National weather service returns XML data with embedded HTML <br> tags
        //These will choke the XML parser as they don't have closing syntax.
        String validXml = asValidXml(weatherXml);
        try {
            weatherParser.parse(new StringReader(validXml));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String asValidXml(String weatherXml) {
        return weatherXml.replaceAll("<br>", "<br/>");
    }

    @Override
    public List<TemperatureItem> getTemperatureItems() {
        ArrayList<TemperatureItem> temperatureItems = new ArrayList<TemperatureItem>();
        List<Map<String, String>> forecast = weatherParser.getLastForecast();
        if (forecast != null) {
            for (Map<String, String> eachEntry : forecast) {
                temperatureItems.add(new TemperatureItem(
                        context.getResources().getDrawable(R.drawable.progress_small),
                        eachEntry.get("iconLink"),
                        eachEntry.get("day"),
                        eachEntry.get("shortDescription"),
                        eachEntry.get("description")
                ));
            }
        }
        return temperatureItems;
    }

    @Override
    public Map<String, String> getCurrentConditions() {
        return weatherParser.getCurrentConditions();
    }

    @Override
    public CharSequence getCity() {
        return weatherParser.getLocation();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private Location getLocation(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ((Activity) context).requestPermissions(new String[]{ACCESS_FINE_LOCATION},0);
            return getDefaultLocation(provider);
        }
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            return location;
        } else {
            return getDefaultLocation(provider);
        }
    }

    @NonNull
    private Location getDefaultLocation(String provider) {
        Location defaultLocation = new Location(provider);
        defaultLocation.setLatitude(DEFAULT_LATITUDE);
        defaultLocation.setLongitude(DEFAULT_LONGITUDE);
        return defaultLocation;
    }
}
